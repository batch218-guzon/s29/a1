//  Find users with letter s in their first name or d in their last name.
 // Use the $or operator.
 //  Show only the firstName and lastName fields and hide the _id field.



db.users.find({$or:[{$or: [{firstName: {$regex: 's'}}, {firstName: {$regex: 'S'}}]}, {$or: [{lastName: {$regex: 'd'}}, {lastName: {$regex: 'D'}}]}]}, {firstName: 1, lastName: 1, _id: 0});





// Find users who are from the HR department and their age is greater then or equal to 70.
// - Use the $and operator




db.users.find({$and:[{department:{$in: ['HR']}}, {age:{$gte:70}}]});






// Find users with the letter e in their first name and has an age of less than or equal to 30.
// - Use the $and, $regex and $lte operators

db.users.find({$and:[{$or:[{firstName: {$regex: 'E'}}, {firstName: {$regex: 'e'}}]}, {age:{$lte:30}}]});